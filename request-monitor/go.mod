module gitlab.com/g-ogawa/cortex/request-monitor

go 1.17

require (
	github.com/prometheus/client_golang v1.9.0
	github.com/stretchr/testify v1.5.1 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
)
